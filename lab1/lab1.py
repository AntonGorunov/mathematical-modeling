import numpy as np
from math import log
import matplotlib.pyplot as plt


def compute_rocket_velocity(fuel_drain_speed, full_rocket_weight, payload_weight, struct_mass_factor):
    """
    Computes velocity of rocket with single stage for given parameters

    :param fuel_drain_speed: originally named u
    :param full_rocket_weight: originally named m0
    :param payload_weight: originally named mp
    :param struct_mass_factor: originally named lambda
    :return: velocity, fuel used, fuel used in %
    """
    rocket_construct_weight = full_rocket_weight * struct_mass_factor
    fuel_weight = full_rocket_weight - payload_weight - rocket_construct_weight
    fuel_used = np.arange(0, fuel_weight, 0.1)
    rocket_velocity = []
    for fuel_use in fuel_used:
        rocket_velocity.append(fuel_drain_speed * log(full_rocket_weight / (full_rocket_weight - fuel_use)))

    fuel_used_percentages = [x / fuel_weight * 100 for x in fuel_used]
    return rocket_velocity, fuel_used, fuel_used_percentages


def compute_multi_stage_rocket_velocity(fuel_drain_speed, full_rocket_weight, payload_weight, struct_mass_factor,
                                        stage_count):
    """
    Compute velocity of multi-stage rocket for given parameters

    """
    stage_weight_factors = [1.0 / stage_count for x in range(0, stage_count)]  # equal weighted stages
    # stage_weight_factors = [2.0 / 3.0, 2.0 / 9.0, 0.1111]  # previous stage weights 2x more than all next stages

    stages_weights = []
    fuel_weights = []
    stages_constructions_weighs = []
    for stage in range(0, stage_count):
        stage_full_weight = (full_rocket_weight - payload_weight) * stage_weight_factors[stage]
        stages_weights.append(stage_full_weight)

        constructions_weight = stage_full_weight * struct_mass_factor

        stages_constructions_weighs.append(constructions_weight)
        fuel_weight = stage_full_weight - constructions_weight
        fuel_weights.append(fuel_weight)

    stages_weights[-1] += payload_weight

    rocket_velocity = []
    total_fuel_used = []
    last_stage_final_velocity = 0.0
    last_stage_fuel_used = 0.0
    dropped_constructions_weight = 0.0

    results = {}

    for stage in range(0, stage_count):
        fuel_used = np.arange(0, fuel_weights[stage], 0.1)
        results[stage] = {'velocity': [], 'spent_fuel': []}

        rocket_weight = sum(stages_weights[stage:])
        for stage_fuel_used in fuel_used:
            velocity = last_stage_final_velocity + fuel_drain_speed * log(
                rocket_weight / (rocket_weight - stage_fuel_used))

            rocket_velocity.append(velocity)
            total_fuel_used.append(last_stage_fuel_used + stage_fuel_used)

            results[stage]['velocity'].append(velocity)
            results[stage]['spent_fuel'].append((last_stage_fuel_used + stage_fuel_used) / sum(fuel_weights) * 100)

        last_stage_final_velocity = rocket_velocity[-1]
        last_stage_fuel_used = sum(fuel_weights[:stage + 1])

    return results


if __name__ == '__main__':
    rocket_velocity, fuel_usage, fuel_used_percent = compute_rocket_velocity(3, 200, 3, 0.1)
    # plt.plot(fuel_used_percent, rocket_velocity, 'r--')

    rocket_velocity, fuel_usage, fuel_used_percent = compute_multi_stage_rocket_velocity(3, 200, 3, 0.1, 3)
    stage_count = 3
    results = compute_multi_stage_rocket_velocity(3, 200, 3, 0.1, stage_count)

    # for i in range(0, stage_count):
    #     plt.plot(results[i]['spent_fuel'], results[i]['velocity'])

    for stage in range(0, stage_count):
        acceleration = np.diff(results[stage]['velocity'])
        plt.plot(results[stage]['spent_fuel'][:-1], acceleration)

    plt.xlabel("Fuel spent, %")
    plt.ylabel("Acceleration, km/s^2")
    # plt.grid()
    # plt.show()

    # plt.xlabel("Fuel spent, %")
    # plt.ylabel("Rocket speed, km/s")
    plt.grid()
    plt.savefig("lab1.png")
    # plt.show()
