import numpy as np
import matplotlib.pyplot as plt
from math import sin, cos, pi
import random
import math

if __name__ == '__main__':
    deviation = 0.003
    rocket_initial_speed = 1000.0
    alpha = pi / 4.0
    g = 9.81
    flight_time = (2 * rocket_initial_speed * sin(alpha) / g)
    x = []
    y = []
    timeline = np.arange(0, math.ceil(flight_time), 1)
    for t in timeline:
        x.append(rocket_initial_speed * t * cos(alpha))
        y.append(rocket_initial_speed * t * sin(alpha) - g * t ** 2 / 2)

    plt.plot(x, y, 'g-')
    plt.grid()
    plt.xlabel("Distance, m")
    plt.ylabel("Height, m")
    plt.savefig("lab2_original_rocket.png")
    plt.clf()

    xe = x[14:40]
    ye = []

    # Calculate rocket points with approximation error
    for i in range(0, 26):
        rnd = random.randint(-100, 100) / 100.0
        ye.append(y[i + 14] * (1 + rnd * deviation))

    xe = np.transpose(xe)
    ye = np.transpose(ye)

    A_matrix_source = []

    for xx in xe:
        A_matrix_source.append([xx ** 2, xx, 1.0])

    # Calculate with Least squares algorithm
    A = np.matrix(A_matrix_source)
    AA = np.transpose(A) * A

    matrix_conditional_number = np.linalg.cond(AA)
    print('Matrix conditional number: ', matrix_conditional_number)

    koef = np.linalg.inv(AA) * np.transpose(A) * ye.reshape(26, 1)
    yet = A * koef
    plt.grid()
    plt.plot(xe, ye, '+', x[14:40], y[14:40], x[14:40], yet)
    plt.xlabel("Distance, m")
    plt.ylabel("Height, m")
    plt.savefig("lab2_approximation.png")
    plt.clf()

    approximation_error = max([abs(ye[i] - y[i + 14]) for i in range(0, len(ye))])
    print('Maximum approximation error: {} meters'.format(approximation_error))

    reconstructed_rocket_trajectory = [[xi ** 2, xi, 1.0] for xi in x] * koef

    plt.plot(x, y, 'r-')  # draw real rocket trajectory
    plt.plot(x, reconstructed_rocket_trajectory, 'g+')  # draw predicated rocket trajectory
    plt.grid()
    plt.xlabel("Distance, m")
    plt.ylabel("Height, m")
    plt.savefig("lab2_reconstructed_rocket_trajectory.png")
    plt.clf()

    # Calculate antirocket trajectory
    matrix_to_list = lambda matrix: [i[0] for i in matrix.tolist()]
    koef_as_list = matrix_to_list(koef)

    vch = math.sqrt(2 * g * (koef_as_list[2] - koef_as_list[1] ** 2 / (4 * koef_as_list[0])))
    vzn = sin(math.atan(koef_as_list[1] ** 2 - 4 * koef_as_list[0] * koef_as_list[2]))
    antirocket_rocket_initial_speed = vch / vzn
    v0x = antirocket_rocket_initial_speed * cos(alpha)
    v0y = antirocket_rocket_initial_speed * sin(alpha)
    L = flight_time * rocket_initial_speed * cos(alpha)
    L1 = 0.75 * L
    u0 = 2000.0
    t0 = 40.0

    a = (v0x * u0 * t0) - (L1 * u0)
    b = (v0y * u0 * t0) - ((g * u0 * (t0 ** 2)) / 2)
    c = (L1 * g * t0) - (L1 * v0y) - ((g * v0x * (t0 ** 2)) / 2)

    antirocket_theta = math.asin(a / math.sqrt(a ** 2 + b ** 2))  # angle of antirocket starts
    bb = antirocket_theta + math.acos(c / math.sqrt(a ** 2 + b ** 2))
    terminate_time = (L1 - v0x * t0) / (v0x + u0 * math.cos(bb))
    u0x = u0 * math.cos(bb)
    u0y = u0 * sin(bb)

    L2 = (v0x * flight_time) * 0.75
    gama = (math.asin((L2 * g / (u0 ** 2)))) / 2
    tp = 2 * u0 * sin(gama) / g

    v1 = 1000.0
    v1x = v1 * cos(alpha)
    v1y = v1 * sin(alpha)

    x_anti_rocket = [L1 - u0x * (t - t0) for t in np.arange(t0, t0 + terminate_time, 0.1)]
    y_anti_rocket = [u0y * (t - t0) - ((g * (t - t0) ** 2) / 2) for t in np.arange(t0, t0 + terminate_time, 0.1)]

    plt.clf()
    plt.plot(x_anti_rocket, y_anti_rocket, 'b.')  # draw anti-rocket

    y_revenge_rocket = [u0 * sin(gama) * (t - t0) - ((g * (t - t0) ** 2) / 2) for t in np.arange(t0, t0 + tp, 0.1)]
    x_revenge_rocket = [L1 - u0 * cos(gama) * (t - t0) for t in np.arange(t0, t0 + tp, 0.1)]
    plt.plot(x_revenge_rocket, y_revenge_rocket, 'g.')  # draw revenge rocket

    br_index = int(math.ceil(t0 + terminate_time))

    plt.plot(x[:br_index], y[:br_index], 'r-')  # draw real rocket trajectory
    plt.plot(x[:br_index], reconstructed_rocket_trajectory[:br_index], 'g+')  # draw predicated rocket trajectory

    xrf = v1x * (t0 + terminate_time)
    yrf = v1y * (t0 + terminate_time) - ((g * (t0 + terminate_time) ** 2) / 2)
    xarf = L1 - u0x * terminate_time
    yarf = u0y * terminate_time - ((g * terminate_time ** 2) / 2)
    rr1 = (xrf - xarf) ** 2
    rr2 = (yrf - yarf) ** 2
    antirocket_error = math.sqrt(rr1 + rr2)
    print("Antirocket error: {} meters".format(antirocket_error))
    if antirocket_error < 100.0:
        print("Antirocket break down enemy's rocket")

    fig = plt.gcf()
    # fig.set_size_inches(10.0, 10.0)
    # plt.axis([0, 1e5, 0, 3 * 1e4])
    plt.xlabel("Distance, m")
    plt.ylabel("Height, m")
    plt.grid()
    plt.savefig("lab2_antirocket.png")
