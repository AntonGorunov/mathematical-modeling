import os
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.integrate import ode
import time


def plot_function(f, begin, end, step, plot_mode):
    x = []
    y = []
    for i in np.arange(begin, end, step):
        x.append(i)
        y.append(f(i))
    plt.plot(x, y, plot_mode)


def integrate(reference_function, differential_equation, t_start, t_final, initial_condition, solver_name,
              figure_name):
    solvers = {
        'ode45': ["dopri5", {}],
        'ode15s': ['vode', {'method': 'bdf', 'order': 15}],

        'dop853': ["dop853", {}],
        'lsoda': ['lsoda', {}]
    }

    if solver_name not in solvers:
        raise Exception("Unknown solver")

    integrator, extra_options = solvers[solver_name]

    step = 0.05
    results = []
    x = []
    approximation_error = []

    tic = time.time()
    solver = ode(differential_equation).set_integrator(integrator, **extra_options)
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        # Integrate the ODE(s)
        solver.integrate(solver.t + step)
        # Store the results to plot later
        x.append(solver.t)
        results.append(solver.y)
        approximation_error.append(abs(reference_function(solver.t) - solver.y))

    elapsed_time = time.time() - tic
    print('Elapsed time: {}'.format(elapsed_time))

    plt.clf()
    plt.grid('on')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title(figure_name)
    # Plot the trajectories:
    plt.plot(x, results, 'r.')
    plot_function(reference_function, t_start, t_final, 0.01, 'g-')
    #
    # plt.clf()
    # plt.plot(x, approximation_error, 'r-')

    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', figure_name + '.png'))
    return elapsed_time, x, approximation_error


if __name__ == '__main__':
    # solver = 'ode15s'
    # solver = 'ode45'
    # solver = 'lsoda'
    solver = 'dop853'

    elapsed_time, ode45_x, ode45_approximation_error = integrate(reference_function=lambda t: t ** 2.0,
                                                                 differential_equation=lambda t, y: 2.0 * t,
                                                                 t_start=0.0,
                                                                 t_final=3.0,
                                                                 initial_condition=[0.0],
                                                                 solver_name='ode45',
                                                                 figure_name='2t')

    elapsed_time, ode15s_x, ode15s_approximation_error = integrate(reference_function=lambda t: t ** 2.0,
                                                                   differential_equation=lambda t, y: 2.0 * t,
                                                                   t_start=0.0,
                                                                   t_final=3.0,
                                                                   initial_condition=[0.0],
                                                                   solver_name='ode15s',
                                                                   figure_name='2t')
    plt.clf()
    plt.plot(ode45_x, ode45_approximation_error, 'r-')
    plt.plot(ode15s_x, ode15s_approximation_error, 'g-')
    plt.savefig('approximation.png')

    integrate(reference_function=lambda t: t ** 2.0,
              differential_equation=lambda t, y: 2.0 * t,
              t_start=0.0,
              t_final=3.0,
              initial_condition=[0.0],
              solver_name=solver,
              figure_name='2t')

    integrate(reference_function=lambda t: t ** 2.0,
              differential_equation=lambda t, y: (2.0 * math.sqrt(y)),
              t_start=0.0,
              t_final=3.0,
              initial_condition=[0.0],
              solver_name=solver,
              figure_name='2sqrt(t)[0,3]')

    integrate(reference_function=lambda t: t ** 2.0,
              differential_equation=lambda t, y: (2.0 * math.sqrt(y)),
              t_start=1.0,
              t_final=3.0,
              initial_condition=[1.0],
              solver_name=solver,
              figure_name='2sqrt(t)[1,3]')

    integrate(reference_function=lambda x: math.cos(x),
              differential_equation=lambda t, y: [-math.sin(t), -y[0]],
              t_start=0.0,
              t_final=2.0 * math.pi,
              initial_condition=np.array([1.0, 0.0]),
              solver_name=solver,
              figure_name='cos(t)')

    integrate(reference_function=lambda x: math.exp(x),
              differential_equation=lambda t, y: math.exp(t),
              t_start=0.0,
              t_final=3.0,
              initial_condition=1.0,
              solver_name=solver,
              figure_name='exp(t)')
