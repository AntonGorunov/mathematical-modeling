import os
import math
import matplotlib.pyplot as plt
from scipy.integrate import ode


def integrate(differential_equation, t_start, t_final, initial_condition):
    step = 0.05
    results = []
    x = []

    solver = ode(differential_equation).set_integrator('dopri5')
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        solver.integrate(solver.t + step)
        x.append(solver.t)
        results.append(solver.y)

    return x, results


def calc_linear_population(birthrate, mortality, name):
    timeline, population = integrate(lambda t, y: (birthrate - mortality) * y, 0, 100, 100)

    plt.clf()
    plt.grid('on')
    plt.xlabel('time')
    plt.ylabel('population')
    plt.plot(timeline, population)
    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'linear_{}.png'.format(name)))


def maltus_non_linear(t, population):
    # birthrate = (1 + t ** 0.014) * (math.sin(population) + 1)
    # mortality = (10 / (1 + t)) * (math.cos(population) + 1) ** 2

    birthrate = max(2 * math.sin(t * math.pi), 0.0)
    mortality = max(0, math.sin(t * math.pi + math.pi)) + 0.2

    return (birthrate - mortality) * population


def calc_non_linear_population():
    timeline, population = integrate(maltus_non_linear, 0, 15, 100)

    plt.clf()
    plt.grid('on')
    plt.xlabel('time (days)')
    plt.ylabel('population')
    plt.plot(timeline, population)
    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'non_linear.png'))


def maltus_equal_weight(t, population):
    a = 0.25
    target_population = 1000.0
    return a * (1 - population / target_population) * population


def calc_equalweight_population():
    timeline, population = integrate(maltus_equal_weight, 0, 100, 10)

    plt.clf()
    plt.grid('on')
    plt.xlabel('time (year)')
    plt.ylabel('population')
    plt.plot(timeline, population)
    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'equal_weight.png'))


if __name__ == '__main__':
    calc_linear_population(0.45, 0.31, 'more')
    calc_linear_population(0.5, 0.5, 'equal')
    calc_linear_population(0.45, 0.8, 'less')

    calc_non_linear_population()
    calc_equalweight_population()
