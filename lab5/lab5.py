import os
from functools import partial
import matplotlib.pyplot as plt
from scipy.integrate import ode
import numpy as np


def integrate(differential_equation, t_start, t_final, initial_condition):
    step = 0.05
    results = []
    x = []

    solver = ode(differential_equation).set_integrator('dopri5')
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        solver.integrate(solver.t + step)
        x.append(solver.t)
        results.append(solver.y)

    return x, results


def predator_prey_population_function(cc, dd, t, y):
    alpha = 20
    beta = 15

    c = cc
    d = dd

    current_prey_population = y[0]
    current_predator_population = y[1]
    scale = 0.5
    return [scale * (alpha - c * current_predator_population) * current_prey_population,
            scale * (-beta + d * current_prey_population) * current_predator_population]


if __name__ == '__main__':
    timeline, population = integrate(partial(predator_prey_population_function, 0.2, 0.3), 0.0, 10.0, [6, 2])
    population = np.array(population)
    prey_population = np.array(population)[:, [0]]
    predator_population = np.array(population)[:, [1]]

    plt.clf()
    plt.grid('on')
    plt.xlabel('Time')
    plt.ylabel('Population')
    plt.plot(timeline, prey_population, 'g-', label='prey')
    plt.plot(timeline, predator_population, 'r-', label='predator')
    plt.legend()
    plt.title("Prey-predator population")
    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'population.png'))

    plt.clf()
    plt.xlabel('Prey')
    plt.ylabel('Predator')
    plt.grid('on')
    plt.title("Prey-predator population correlation")
    t = True

    plt.plot(prey_population, predator_population, 'r-')

    plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'prey_predator_correlation.png'))
    plt.show()
