import os
import struct
import matplotlib.pyplot as plt
from scipy.integrate import ode
import numpy as np


def randf() -> float:
    return abs(struct.unpack('<i', os.urandom(4))[0]) / 2 ** (8 * 4)


def integrate(differential_equation, t_start, t_final, initial_condition):
    step = 0.05
    results = []
    x = []

    solver = ode(differential_equation).set_integrator('dopri5')
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        solver.integrate(solver.t + step)
        x.append(solver.t)
        results.append(solver.y)

    return x, results


if __name__ == '__main__':
    weapon_count_factor_c1 = 3.0
    weapon_count_factor_c2 = 3.0

    obsolescence_factor_c1 = 4.0
    obsolescence_factor_c2 = 4.0

    mistrust_factor_c1 = 5.0
    mistrust_factor_c2 = 5.0

    M10 = (weapon_count_factor_c1 * mistrust_factor_c1 + obsolescence_factor_c2 * mistrust_factor_c1) / \
          (obsolescence_factor_c1 * obsolescence_factor_c2 - weapon_count_factor_c1 * weapon_count_factor_c2)

    M20 = (weapon_count_factor_c2 * mistrust_factor_c1 + obsolescence_factor_c2 * mistrust_factor_c1) / \
          (obsolescence_factor_c1 * obsolescence_factor_c2 - weapon_count_factor_c1 * weapon_count_factor_c2)


    def fun1(t, Y):
        return [weapon_count_factor_c1 * Y[1] - obsolescence_factor_c1 * Y[0] + mistrust_factor_c1,
                weapon_count_factor_c2 * Y[0] - obsolescence_factor_c2 * Y[1] + mistrust_factor_c2]


    for i in range(0, 5):
        M0 = randf() * 2.0 * M10
        N0 = randf() * 2.0 * M20
        [t, Y] = integrate(fun1, 0.0, 10.0, [M0, N0])
        m1 = np.array(Y)[:, [0]]
        m2 = np.array(Y)[:, [1]]
        plt.plot(m1, m2)

        # plt.plot(t, m2, 'r-')
        # plt.plot(t, m1, 'g-')

    plt.grid('on')
    plt.show()
