import os
import struct
from math import sin, cos, pi
import matplotlib.pyplot as plt
from scipy.integrate import ode
import numpy as np


def randf() -> float:
    return abs(struct.unpack('<i', os.urandom(4))[0]) / 2 ** (8 * 4)


def integrate(differential_equation, t_start: float, t_final: float, initial_condition):
    step = 0.1
    results = []
    x = []

    solver = ode(differential_equation).set_integrator('dopri5')
    solver.set_initial_value(initial_condition, t_start)

    eps = 0.0000001
    while solver.successful() and solver.t < t_final - eps:
        solver.integrate(solver.t + step)
        x.append(solver.t)
        results.append(solver.y)

    return x, results


if __name__ == '__main__':
    non_military_death_factor_c1 = 0.3
    non_military_death_factor_c2 = 0.2

    military_death_factor_c1 = 0.7
    military_death_factor_c2 = 0.8

    reinforcement_speed_c1 = 1
    reinforcement_speed_c2 = 1

    end_time = 10.0
    samples_count = 10

    M10 = M20 = 200

    plt.axis([0, M10, 0, M20])
    plt.hold('on')


    def calc_model(t: float, army_strength: np.array) -> np.array:
        if army_strength[0] < 0 or army_strength[1] < 0:
            return [0.0, 0.0]

        return [-non_military_death_factor_c1 * army_strength[0] -
                military_death_factor_c1 * army_strength[1] + reinforcement_speed_c1,

                -non_military_death_factor_c2 * army_strength[1] -
                military_death_factor_c2 * army_strength[0] + reinforcement_speed_c2]


    for i in range(0, samples_count):
        M0 = cos(pi / 2.0 * i / samples_count) * M10
        N0 = sin(pi / 2.0 * i / samples_count) * M20

        [t, Y] = integrate(calc_model, 0.0, end_time, [M0, N0])
        m1 = np.array(Y)[:, [0]]
        m2 = np.array(Y)[:, [1]]
        plt.plot(m1, m2)
        plt.xlabel('First army')
        plt.ylabel('Second army')

        # plt.plot(t, m2, 'r-')
        # plt.plot(t, m1, 'g-')

    plt.grid('on')
    # plt.savefig(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'images', 'country_fighting.png'))
    plt.show()
